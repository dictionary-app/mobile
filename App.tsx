import { GlobalStateProvider } from './src/providers/GlobalStateProvider';
import Navigation from './src/navigation/Navigation';
import { DictionaryProvider } from './src/providers/DictionaryProvider';
import 'react-native-url-polyfill/auto';

export default function App() {
  return (
    <GlobalStateProvider>
      <DictionaryProvider>
          <Navigation />
      </DictionaryProvider>
    </GlobalStateProvider>
  );
}

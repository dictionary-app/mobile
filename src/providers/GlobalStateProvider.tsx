import {
  createContext,
  Dispatch,
  FC,
  SetStateAction,
  useMemo,
  useState,
} from 'react';
import { UserEntity } from '../api';

interface IContext {
  user: UserEntity | null;
  setUser: Dispatch<SetStateAction<UserEntity | null>>;
  isLoadingInitialData: boolean;
  setIsLoadingInitialData: Dispatch<SetStateAction<boolean>>;
}

export const GlobalStateContext = createContext<IContext>({} as IContext);

export const GlobalStateProvider: FC = ({ children }) => {
  const [user, setUser] = useState<UserEntity | null>(null);
  const [isLoadingInitialData, setIsLoadingInitialData] =
    useState<boolean>(true);

  const memoValue = useMemo(
    () => ({
      user,
      setUser,
    }),
    [user],
  );

  return (
    <GlobalStateContext.Provider
      value={{ ...memoValue, setIsLoadingInitialData, isLoadingInitialData }}
    >
      {children}
    </GlobalStateContext.Provider>
  );
};

import { GlobalStateContext } from '../GlobalStateProvider';
import { useContext } from 'react';

export const useGlobalStateProvider = () => useContext(GlobalStateContext);

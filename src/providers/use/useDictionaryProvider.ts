import { DictionaryContext } from '../DictionaryProvider';
import { useContext } from 'react';

export const useDictionaryProvider = () => useContext(DictionaryContext);

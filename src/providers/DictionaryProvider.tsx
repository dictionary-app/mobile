import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {
  createContext,
  Dispatch,
  FC,
  SetStateAction,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { Alert } from 'react-native';
import { DictionaryItemDto } from '../api';
import {
  addWordToDictionary,
  findAllDictionary,
  removeWordFromDictionary,
  synhronizeDictionary,
} from '../api/dictionary';
import { IResponseAnswer, ResponseStatus } from '../api/types';
import { checkCyrillic } from '../utils/checkCyrillic';
import { useGlobalStateProvider } from './use/useGlobalStateProvider';

interface IContext {
  dictionaryItems: DictionaryItemDto[] | null;
  filteredItems: DictionaryItemDto[] | null;
  addDictionaryItem: (item: DictionaryItemDto) => Promise<number | undefined>;
  removeDictionaryItem: (item: DictionaryItemDto) => void;
  getDictionary: () => Promise<IResponseAnswer>;
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
  isDictionatyLoading: boolean;
}

export const DictionaryContext = createContext<IContext>({} as IContext);

export const DictionaryProvider: FC = ({ children }) => {
  const [isDictionatyLoading, setIsDictionaryLoading] = useState(true);
  const [dictionaryItems, setDictionaryItems] = useState<
    DictionaryItemDto[] | null
  >(null);
  const [filteredItems, setFilteredItems] = useState<
    DictionaryItemDto[] | null
  >(null);

  const [search, setSearch] = useState('');

  const { user } = useGlobalStateProvider();

  const SYNHRONIZE_KEY = `synhronize=${user?.id}`;

  const lifetime = 24 * 60 * 60 * 3;

  useEffect(() => {
    if (!dictionaryItems) return;
    const langValueOf = checkCyrillic(search) ? 'translateValue' : 'wordValue';

    const items = search.length
      ? dictionaryItems?.filter(
          (item) =>
            item[langValueOf]
              .toLocaleLowerCase()
              .indexOf(search.toLowerCase()) !== -1,
        )
      : dictionaryItems;

    setFilteredItems(items);
  }, [search, dictionaryItems]);

  /** Получение слов из словаря */
  const getDictionary = async () => {
    setIsDictionaryLoading(true);
    try {
      const now = Date.now();
      const timeOfLastUpdated = await AsyncStorage.getItem(SYNHRONIZE_KEY);
      if (!timeOfLastUpdated || now - parseInt(timeOfLastUpdated) > lifetime) {
        await synhronizeDictionary();
        await AsyncStorage.setItem(SYNHRONIZE_KEY, now.toString());
      }

      const response = await findAllDictionary();
      if (response !== null) setDictionaryItems(response);
      return { status: ResponseStatus.SUCCESS };
    } catch (err: any) {
      Alert.alert('Ошбка получения словаря', err);
      return { status: ResponseStatus.ERROR };
    } finally {
      setIsDictionaryLoading(false);
    }
  };

  /** Добавление нового слова в словарь */
  const addDictionaryItem = async (
    item: DictionaryItemDto,
  ): Promise<number | undefined> => {
    try {
      const id = await addWordToDictionary(item);
      await getDictionary();
      return id;
    } catch (err: any) {
      Alert.alert('Ошибка добавления слова в словарь', err);
    }
  };

  /** Удаление слова из словаря */
  const removeDictionaryItem = (item: DictionaryItemDto) => {
    Alert.alert('Удалить слово ?', '', [
      {
        text: 'Нет',
        onPress: () => null,
      },
      {
        text: 'Да',
        onPress: async () => {
          await removeWordFromDictionary({
            localId: item.localId,
            id: item.id,
          });
          await getDictionary();
        },
      },
    ]);
  };

  const store = useMemo(
    () => ({
      dictionaryItems,
      filteredItems,
    }),
    [dictionaryItems, filteredItems],
  );

  return (
    <DictionaryContext.Provider
      value={{
        ...store,
        ...{
          addDictionaryItem,
          removeDictionaryItem,
          getDictionary,
          search,
          setSearch,
          isDictionatyLoading,
        },
      }}
    >
      {children}
    </DictionaryContext.Provider>
  );
};

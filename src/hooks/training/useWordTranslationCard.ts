import { useCallback, Dispatch, SetStateAction, useRef, useState } from 'react';
import { Animated, PanResponder } from 'react-native';
import { DictionaryItemDto } from '../../api';
import { ACTION_OFFSET, CARD } from '../../styles/card';
import { useKeyboard } from '../instruments/useKeyboard';
import { useSound } from '../useSound';

export const useWordTranslationCard = (
  item: DictionaryItemDto,
  setLocalItems: Dispatch<SetStateAction<DictionaryItemDto[]>>,
  setAnswers: Dispatch<SetStateAction<number>>,
  setTrueAnswers: Dispatch<SetStateAction<number>>,
) => {
  enum AnswerType {
    TRUE_ANSWER = 'TRUE_ANSWER',
    FALSE_ANSWER = 'FALSE_ANSWER',
  }

  const [onLongMove, setOnLongMove] = useState(false);
  const swipe = useRef(new Animated.ValueXY()).current;
  const titlSign = useRef(new Animated.Value(1)).current;
  const { playSound, isPlaying } = useSound(item.pronunciation);
  const { isOpenKeyboard } = useKeyboard();
  const [translateValue, setTranslateValue] = useState<string>('');
  const [isValidInput, setIsValidInput] = useState<boolean>(true);
  const [isFalseAnswer, setIsFalseAnswer] = useState<boolean>(false);

  const trueAnswerHandler = (dy?: number) => {
    setAnswers((prevState) => prevState + 1);
    setTrueAnswers((prevState) => prevState + 1);

    Animated.timing(swipe, {
      duration: 1000,
      toValue: {
        x: 1000,
        y: dy || 0,
      },
      useNativeDriver: true,
    }).start(() => swipeHandler());
  };

  const falseAnswerHandler = (dy?: number) => {
    setAnswers((prevState) => prevState + 1);

    Animated.timing(swipe, {
      duration: 1000,
      toValue: {
        x: -1000,
        y: dy || 0,
      },
      useNativeDriver: true,
    }).start(() => swipeHandler());
  };

  const panResponder = PanResponder.create({
    onMoveShouldSetPanResponder: () => onLongMove && !isFalseAnswer,
    onPanResponderMove: (_, { dx, dy, y0 }) => {
      swipe.setValue({ x: dx, y: dy });
      titlSign.setValue(y0 * 0.6 > CARD.HEIGHT / 2 ? 1 : -1);
    },
    onPanResponderRelease: (_, { dx, dy }) => {
      const direction = Math.sign(dx);
      const isActionActive = Math.abs(dx) > ACTION_OFFSET;
      const directionType =
        direction === 1 ? AnswerType.TRUE_ANSWER : AnswerType.FALSE_ANSWER;

      if (isActionActive) {
        directionType === AnswerType.TRUE_ANSWER
          ? trueAnswerHandler()
          : falseAnswerHandler();
      } else {
        Animated.spring(swipe, {
          toValue: {
            x: 0,
            y: 0,
          },
          useNativeDriver: true,
          friction: 5,
        }).start();
      }
    },
  });

  const swipeHandler = useCallback(() => {
    setLocalItems((prevState) => prevState?.slice(1));
    swipe.setValue({ x: 0, y: 0 });
  }, [swipe]);

  const rotate = Animated.multiply(swipe.x, titlSign).interpolate({
    inputRange: [-ACTION_OFFSET, 0, ACTION_OFFSET],
    outputRange: ['8deg', '0deg', '-8deg'],
  });

  const animatedCardStyle = {
    transform: [...swipe.getTranslateTransform(), { rotate }],
  };

  const knowOpacity = swipe.x.interpolate({
    inputRange: [20, ACTION_OFFSET],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  });

  const dontKnowOpacity = isFalseAnswer
    ? 1
    : swipe.x.interpolate({
        inputRange: [-ACTION_OFFSET, -20],
        outputRange: [1, 0],
        extrapolate: 'clamp',
      });

  const checkWordTranslateHandler = () => {
    if (translateValue.toLowerCase() === item.wordValue) {
      setIsValidInput(true);
      trueAnswerHandler();
    } else {
      setIsValidInput(false);
      setIsFalseAnswer(true);
    }
  };

  const checkTranslateWordHandler = () => {
    if (translateValue.toLowerCase() === item.translateValue) {
      setIsValidInput(true);
      trueAnswerHandler();
    } else {
      setIsValidInput(false);
      setIsFalseAnswer(true);
    }
  };

  const dontKnowHandler = () => {
    setIsValidInput(false);
    setIsFalseAnswer(true);
  };

  const nextHandler = () => {
    setIsValidInput(true);
    falseAnswerHandler();
  };

  return {
    animatedCardStyle,
    panResponder,
    swipe,
    titlSign,
    dontKnowOpacity,
    knowOpacity,
    isOpenKeyboard,
    isFalseAnswer,
    isPlaying,
    translateValue,
    isValidInput,
    checkWordTranslateHandler,
    checkTranslateWordHandler,
    dontKnowHandler,
    nextHandler,
    setTranslateValue,
    playSound,
    trueAnswerHandler,
    falseAnswerHandler,
    setOnLongMove,
  };
};

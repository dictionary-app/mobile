import { useEffect, useMemo, useState } from 'react';
import { DictionaryItemDto } from '../../api';

export const useWordTranslationContent = (items: DictionaryItemDto[]) => {
  const [localItems, setLocalItems] = useState<DictionaryItemDto[]>(items);
  const [answers, setAnswers] = useState<number>(0);
  const [trueAnswers, setTrueAnswers] = useState<number>(0);

  useEffect(() => {
    if (!localItems?.length) setLocalItems(items);
  }, [localItems?.length, items]);

  const trueAnswersPercent = useMemo(() => {
    return trueAnswers ? Math.floor((trueAnswers / answers) * 100) : 0;
  }, [answers, trueAnswers]);

  return {
    localItems,
    trueAnswersPercent,
    setLocalItems,
    setAnswers,
    setTrueAnswers,
  };
};

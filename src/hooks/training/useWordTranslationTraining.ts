import { DICTIONARY_GROUP } from './../../models/GroupsModel';
import { useDictionaryProvider } from '../../providers/use/useDictionaryProvider';
import {
  useIsFocused,
  useRoute,
  useNavigation,
} from '@react-navigation/native';
import { useEffect } from 'react';
import { trainings } from '../../config/training';
import { useGroups } from '../groups/useGroups';

export const useWordTranslationTraining = () => {
  const route = useRoute();
  const { navigate } = useNavigation();

  const isFocused = useIsFocused();
  const { dictionaryItems, getDictionary } = useDictionaryProvider();
  const { words: groupWords, getWordsByGroup } = useGroups();

  // @ts-ignore
  const trainingAlias = route.params?.trainingAlias;
  const training = trainings.find(
    (training) => training.alias === trainingAlias,
  );

  // @ts-ignore
  const groupId = route.params?.groupId;

  const words = groupId === DICTIONARY_GROUP ? dictionaryItems : groupWords;

  const backClickHandler = () =>
    navigate('WordTranslationGroups', { trainingAlias });

  useEffect(() => {
    if (isFocused && groupId) {
      groupId === DICTIONARY_GROUP ? getDictionary() : getWordsByGroup(groupId);
    }
  }, [isFocused]);

  return {
    words,
    training,
    backClickHandler,
  };
};

import { Audio } from 'expo-av';
import { useEffect, useState } from 'react';

export const useSound = (uri: string | undefined) => {
  const [sound, setSound] = useState<any>();

  const [isPlaying, setIsPlaying] = useState(false);

  async function playSound() {
    if (uri) {
      const source = { uri };
      const { sound } = await Audio.Sound.createAsync(source);

      setSound(sound);
      const response = await sound.playAsync();

      // @ts-ignore
      if (!response.error) {
        setIsPlaying(true);

        // @ts-ignore
        setTimeout(() => setIsPlaying(false), response.playableDurationMillis);
      }
    }
  }

  useEffect(() => {
    return sound
      ? () => {
          sound.unloadAsync();
        }
      : undefined;
  }, [sound]);

  return { playSound, isPlaying };
};

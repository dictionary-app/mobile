import { useEffect, useState } from 'react';
import { getSearchApi } from '../../api/dictionary';
import { useRoute } from '@react-navigation/native';
import { checkCyrillicDictionaryItem } from '../../utils/checkCyrillic';
import { DictionaryItemDto } from '../../api';

export const useSearch = () => {
  const route = useRoute();

  const [search, setSearch] = useState('');

  const [itemCandidate, setItemCandidate] = useState<DictionaryItemDto | null>(
    null,
  );

  const [items, setItems] = useState<DictionaryItemDto[]>([]);

  /** Методы */
  const searchHandler = async () => {
    const items = await getSearchApi(search);
    setItems(items);
  };

  const reverseSearchWord = async (item:DictionaryItemDto):Promise<DictionaryItemDto | false> => {
    try {
      await setSearch(item.translateValue)
      const items = await searchDictionaryHandler(item.translateValue)
      const candidate = items.find((c:DictionaryItemDto) => c.translateValue === item.wordValue)
      if(candidate) {
        await setItemCandidate(candidate)
        return candidate
      } 
      return false
    } catch(err) {
      return false
    }
  }

  const searchDictionaryHandler = async (searchValue: string) => {
    const items = await getSearchApi(searchValue);
    setItems(items);
    return items
  };

  const clearHandler = () => {
    setItems([]);
    setItemCandidate(null);
    setSearch('');
  };

  // @ts-ignore
  const routSearchValue = route.params?.search;

  const checkRouteParams = () => {
    if (routSearchValue) {
      setSearch(routSearchValue);
      searchDictionaryHandler(routSearchValue);
    }
  };
  

  useEffect(() => {
    return routSearchValue ? checkRouteParams() : undefined;
  }, [routSearchValue]);

  return {
    search,
    setSearch,
    items,
    searchHandler,
    reverseSearchWord,
    clearHandler,
    setItemCandidate,
    itemCandidate,
  };
};

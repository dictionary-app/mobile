import {
  useIsFocused,
  useNavigation,
  useRoute,
} from '@react-navigation/native';
import { useEffect, useState } from 'react';
import { ResponseStatus } from '../../api/types';
import { Alert } from 'react-native';
import { DictionaryItemDto } from '../../api';
import {
  addWordToGroup,
  createGroupApi,
  findAllWordsByGroup,
  getGroupByIdApi,
  removeWordFromGroup,
  updateGroupApi,
} from '../../api/groups';
import { checkCyrillic } from '../../utils/checkCyrillic';

export const useGroupItem = () => {
  /**
   ****************************************************** ИНИЦИАЛИЗАЦИФ ************************************************
   */

  const route = useRoute();
  const { navigate } = useNavigation();
  // @ts-ignore
  const id = route.params?.id;

  /**
   ****************************************************** СТЕЙТ ХУКА ***************************************************
   */
  const isFocused = useIsFocused();
  const [title, setTitle] = useState('');
  const [words, setWords] = useState<DictionaryItemDto[]>([]);
  const [isOpenSearch, setIsOpenSearch] = useState(false);
  const [search, setSearch] = useState('');
  const [filteredWords, setFilteredWords] = useState<DictionaryItemDto[]>([]);
  const [isLoadingGroup, setIsLoadingGroup] = useState(false);

  /**
   ****************************************************** ВЫЧИСЛЯЕМЫЕ СВОЙСТВА *****************************************
   */

  /**
   * Заголовок страницы набора
   * @returns string
   */
  const headerTitle = () =>
    isOpenSearch ? 'Слова из словарика' : 'Редактирование набора';

  /**
   * Иконка для кнопки добавления
   * @returns string
   */
  const iconNameAddButton = () => (isOpenSearch ? 'save' : 'plus');

  /**
   *********************************************************** МЕТОДЫ ***********************************************************
   */

  /**
   * Обработчик открытия/закрытия формы добавления новых слов в набор
   * @returns () => void
   */
  const addButtonHandler = async () => {
    if (isOpenSearch) {
      setIsOpenSearch(false);
    } else {
      setIsOpenSearch(true);
    }
  };

  /**
   * Обработчик нажатия кнопки назад в шапке
   * @returns () => void
   */
  const onPressBackButton = () =>
    isOpenSearch ? setIsOpenSearch(false) : navigate('Groups');

  /**
   * Получение объекта группы по id
   * @param {number} id - id группы
   */
  const getGroupById = async (id: number): Promise<void> => {
    setIsLoadingGroup(true);
    try {
      const item = await getGroupByIdApi(id);
      const words = await findAllWordsByGroup(item.id);
      setWords(words);
      setTitle(item.title);
    } catch (err: any) {
      Alert.alert('Ошибка получения группы', err);
    } finally {
      setIsLoadingGroup(false);
    }
  };

  /**
   *
   * @param {DictionaryItemDto} item - объект слова из словаря
   * @returns () => void
   */
  const addRemoveWordItem = (item: DictionaryItemDto): void => {
    if (words?.find((i) => i.localId === item.localId)) {
      removeWord(item);
    } else {
      addWord(item);
    }
  };

  /**
   *
   * @param {DictionaryItemDto} item - объект слова из словаря
   * @returns () => void
   */
  const addWord = async (item: DictionaryItemDto) => {
    try {
      await addWordToGroup({
        dictionaryId: item.localId,
        groupId: id,
      });
      setWords([...words, item]);
    } catch (err: any) {
      Alert.alert('Ошибка добавления слова', err);
    }
  };

  /**
   *
   * @param {DictionaryItemDto} item - объект слова из словаря
   * @returns () => void
   */
  const removeWord = async (item: DictionaryItemDto) => {
    try {
      await removeWordFromGroup({
        dictionaryId: item.localId,
        groupId: id,
      });
      const result = words.filter((i) => i.localId !== item.localId);
      setWords(result);
    } catch (err: any) {
      Alert.alert('Ошибка удаления слова', err);
    }
  };

  const onCreateHandler = async () => {
    try {
      if (!title.length) {
        Alert.alert('Задайте имя набора');
        return;
      }
      const { id } = await createGroupApi({ title });
      navigate('GroupItem', { id });
      return { status: ResponseStatus.SUCCESS };
    } catch (err: any) {
      Alert.alert('Ошибка добавления слова в словарь', err);
      return { status: ResponseStatus.ERROR };
    }
  };

  const onUpdateHandler = async () => {
    try {
      await updateGroupApi(id, { title });

      return { status: ResponseStatus.SUCCESS };
    } catch (err: any) {
      Alert.alert('Ошибка добавления слова в словарь', err);
      return { status: ResponseStatus.ERROR };
    }
  };

  useEffect(() => {
    if (isFocused && id) getGroupById(id);
  }, [isFocused]);

  useEffect(() => {
    if (!words) return;

    const langValueOf = checkCyrillic(search) ? 'translateValue' : 'wordValue';

    const result = words?.filter(
      (item) =>
        item[langValueOf].toLocaleLowerCase().indexOf(search.toLowerCase()) !==
        -1,
    );

    setFilteredWords(result);
  }, [words, search]);

  return {
    id,
    title,
    setTitle,
    search,
    setSearch,
    filteredWords,
    addButtonHandler,
    onPressBackButton,
    isOpenSearch,
    headerTitle,
    iconNameAddButton,
    getGroupById,
    words,
    addRemoveWordItem,
    removeWord,
    onCreateHandler,
    isLoadingGroup,
    onUpdateHandler,
  };
};

import { useState } from 'react';
import { Alert } from 'react-native';
import { ResponseStatus } from '../../api/types';
import {
  findAllWordsByGroup,
  getGroupsApi,
  removeGroupApi,
} from '../../api/groups';
import { DictionaryItemDto, GroupEntity } from '../../api';

export const useGroups = () => {
  const [groups, setGroups] = useState<GroupEntity[]>([]);
  const [words, setWords] = useState<DictionaryItemDto[]>([]);
  const [isLoadingGroups, setIsLoadingGroups] = useState(false);

  /** Методы */

  /** Получение набора слов */
  const getGroups = async () => {
    setIsLoadingGroups(false);
    try {
      const response = await getGroupsApi();
      setGroups(response);
      return { status: ResponseStatus.SUCCESS };
    } catch (err: any) {
      Alert.alert('Ошбка получения набора слов', err);
      return { status: ResponseStatus.ERROR };
    } finally {
      setIsLoadingGroups(true);
    }
  };

  /** Получение набора слов */
  const getWordsByGroup = async (id: number) => {
    try {
      const words = await findAllWordsByGroup(id);
      setWords(words);
      return { status: ResponseStatus.SUCCESS };
    } catch (err: any) {
      Alert.alert('Ошбка получения набора слов', err);
      return { status: ResponseStatus.ERROR };
    }
  };

  const onRemove = async (group: GroupEntity) => {
    Alert.alert('Удалить набор слов ?', '', [
      {
        text: 'Нет',
        onPress: () => null,
      },
      {
        text: 'Да',
        onPress: async () => {
          try {
            await removeGroupApi(group.id);
            const items = groups?.filter((item) => item.id !== group.id);
            setGroups(items || []);
            return { status: ResponseStatus.SUCCESS };
          } catch (err: any) {
            Alert.alert('Ошибка добавления слова в словарь', err);
            return { status: ResponseStatus.ERROR };
          }
        },
      },
    ]);
  };

  return {
    groups,
    words,
    getGroups,
    setGroups,
    onRemove,
    getWordsByGroup,
    isLoadingGroups,
  };
};

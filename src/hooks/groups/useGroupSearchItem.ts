import { useDictionaryProvider } from '../../providers/use/useDictionaryProvider';
import { useEffect, useMemo, useState } from 'react';
import { DictionaryItemDto } from '../../api';

export const useGroupSearchItem = () => {
  const [dictionarySearch, setDictionarySearch] = useState('');
  const [dictionaryFilteredItems, setDictionaryFilteredItems] = useState<
    DictionaryItemDto[]
  >([]);
  const { dictionaryItems } = useDictionaryProvider();

  /** Методы */

  useEffect(() => {
    if (!dictionaryItems) return;
    const result = dictionarySearch.length
      ? dictionaryItems?.filter(
          (item) =>
            item.wordValue
              .toLocaleLowerCase()
              .indexOf(dictionarySearch.toLowerCase()) !== -1,
        )
      : dictionaryItems;

    setDictionaryFilteredItems(result);
  }, [dictionarySearch, dictionaryItems]);

  /** Получение слов из словаря */

  const cachedItems = useMemo(
    () => ({
      dictionaryFilteredItems,
    }),
    [dictionaryItems, dictionaryFilteredItems],
  );

  return {
    dictionarySearch,
    setDictionarySearch,
    ...cachedItems,
  };
};

import { useEffect, useState } from "react";
import { Keyboard } from "react-native";

export const useKeyboard =()=> {
    const [isOpenKeyboard, setIsOpenKeyboard] = useState<boolean>(false);

    useEffect(() => {
      const showSubscription = Keyboard.addListener("keyboardDidShow", () => {
        setIsOpenKeyboard(true);
      });
      const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
        setIsOpenKeyboard(false);
      });
  
      return () => {
        showSubscription.remove();
        hideSubscription.remove();
      };
    }, []);

    return {
        isOpenKeyboard
    }
}
import {
  NavigationContainer,
  useNavigationContainerRef,
} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, { FC, useEffect, useState } from 'react';
import Profile from '../screens/profile/ProfileScreen';
import Dictionary from '../screens/dictionary/DictionaryScreen';
import Search from '../screens/search/SearchScreen';
import Groups from '../screens/groups/GroupsScreen';
import CreateGroup from '../screens/groups/CreateGroupScreen';

import GroupsItem from '../screens/groups/GroupsItemScreen';

import Auth from '../screens/auth/AuthScreen';
import Register from '../screens/auth/RegisterScreen';

import Footer from '../layouts/main/footer/Footer';
import TrainingScreen from '../screens/training/TrainingScreen';
import WordTranslationTrainingScreen from '../screens/training/WordTranslation/WordTranslationTrainingScreen';
import WordTranslationGroupsScreen from '../screens/training/WordTranslation/WordTranslationGroupsScreen';
import { useGlobalStateProvider } from '../providers/use/useGlobalStateProvider';
import { getUserByTokenApi } from '../api/users';

const Stack = createNativeStackNavigator();

const Navigation: FC = () => {
  const ref = useNavigationContainerRef();

  const [name, setName] = useState<string | undefined>(undefined);

  const { user, setUser, setIsLoadingInitialData } = useGlobalStateProvider();

  useEffect(() => {
    setName(ref.getCurrentRoute()?.name);
    const listener = ref.addListener('state', () =>
      setName(ref.getCurrentRoute()?.name),
    );
    return () => {
      ref.removeListener('state', listener);
    };
  }, []);

  const checkToken = async () => {
    try {
      const localUser = await getUserByTokenApi();
      if (localUser) {
        await setUser(localUser);
        ref.navigate('Dictionary');
      } else {
        ref.navigate('Auth');
      }
    } catch (e: any) {
      ref.navigate('Auth');
    } finally {
      setIsLoadingInitialData(false);
    }
  };

  useEffect(() => {
    checkToken();
  }, []);

  return (
    <>
      <NavigationContainer ref={ref}>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          {user ? (
            getAuthenticatedScreens()
          ) : (
            <>
              <Stack.Screen name="Auth" component={Auth} />
              <Stack.Screen name="Register" component={Register} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
      {user && name && <Footer navigate={ref.navigate} currentRoute={name} />}
    </>
  );
};

export default Navigation;

function getAuthenticatedScreens() {
  return (
    <>
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Dictionary" component={Dictionary} />
      <Stack.Screen name="Search" component={Search} />
      <Stack.Screen name="Groups" component={Groups} />
      <Stack.Screen name="CreateGroup" component={CreateGroup} />
      <Stack.Screen name="GroupItem" component={GroupsItem} />
      <Stack.Screen name="Training" component={TrainingScreen} />
      <Stack.Screen
        name="WordTranslationGroups"
        component={WordTranslationGroupsScreen}
      />
      <Stack.Screen
        name="WordTranslationTraining"
        component={WordTranslationTrainingScreen}
      />
    </>
  );
}

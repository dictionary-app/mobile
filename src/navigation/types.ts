export type TypeRootStackParamList = {
  Auth: undefined;
  Profile: undefined;
  Register: undefined;
  Dictionary: undefined;
  Search?: {
    search: string;
    groupId?: number;
  };
  /** Groups */
  Groups: undefined;
  CreateGroup: undefined;
  GroupItem: {
    id: number;
  };
  /** Training */
  Training: undefined;
  /** Plain training */
  WordTranslationGroups?: {
    trainingAlias: string;
  };
  WordTranslationTraining?: {
    groupId: number | string;
    trainingAlias: string;
  };
};

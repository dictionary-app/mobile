import {
  CancelablePromise,
  CreateGroupDictionaryDto,
  CreateGroupDto,
  DictionaryModel,
  GroupEntity,
  GroupModel,
  GroupsService,
  RemoveGroupDictionaryDto,
  UpdateGroupDto,
} from './index';
import { OpenAPI } from './index';
import { API_HOST } from '../config/global';

OpenAPI.BASE = API_HOST;

export const getGroupsApi = (): CancelablePromise<Array<GroupEntity>> => {
  return GroupsService.groupsControllerFindAll();
};

export const getGroupByIdApi = (id: number): CancelablePromise<GroupModel> => {
  return GroupsService.groupsControllerFindOne(id);
};

export const createGroupApi = (
  request: CreateGroupDto,
): CancelablePromise<GroupModel> => {
  return GroupsService.groupsControllerCreate(request);
};

export const updateGroupApi = (
  id: number,
  request: UpdateGroupDto,
): CancelablePromise<any> => {
  return GroupsService.groupsControllerUpdate(id, request);
};

export const removeGroupApi = (id: number): CancelablePromise<number> => {
  return GroupsService.groupsControllerRemove(id);
};

export const addWordToGroup = (
  item: CreateGroupDictionaryDto,
): CancelablePromise<any> => {
  return GroupsService.groupsControllerAddWordToGroup(item);
};

export const removeWordFromGroup = (
  item: RemoveGroupDictionaryDto,
): CancelablePromise<number> => {
  return GroupsService.groupsControllerRemoveWordFromGroup(item);
};

export const findAllWordsByGroup = (
  id: number,
): CancelablePromise<Array<DictionaryModel>> => {
  return GroupsService.groupsControllerFindAllWordsByGroup(id);
};

import { UserEntity } from './../../generated/api/models/UserEntity';
import {
  CancelablePromise,
  UpdateUserDto,
  UsersService,
} from './index';
import { OpenAPI } from './index';
import { API_HOST } from '../config/global';

OpenAPI.BASE = API_HOST;

export const getUserByTokenApi = (): CancelablePromise<UserEntity> => {
  return UsersService.usersControllerGetUserByToken();
};

export const updateUser = (id: number, user: UpdateUserDto): CancelablePromise<UserEntity> => {
  return UsersService.usersControllerUpdate(id, user);
};

export enum ResponseStatus {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
}

export interface IResponseAnswer {
  status: ResponseStatus;
  message?: string;
}

import { API_HOST } from '../config/global';
import { OpenAPI } from './index';

OpenAPI.BASE = API_HOST;

import {
  AuthService,
  CancelablePromise,
  LoginRequestDto,
  LoginResponseDto,
} from './index';

export const loginApi = (
  requestBody: LoginRequestDto,
): CancelablePromise<LoginResponseDto> => {
  return AuthService.authControllerLogin(requestBody);
};

export const logoutApi = (): CancelablePromise<void> => {
  return AuthService.authControllerLogout();
};

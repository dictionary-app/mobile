import { API_HOST } from '../config/global';
import { DictionaryItemDto } from './../../generated/api/models/DictionaryItemDto';
import { DictionaryService } from './../../generated/api/services/DictionaryService';
import {
  CancelablePromise,
  OpenAPI,
  RemoveDictionaryRequestDto,
} from './index';

OpenAPI.BASE = API_HOST;

export const getSearchApi = (text: string): CancelablePromise<Array<any>> => {
  return DictionaryService.dictionaryControllerSearch(text);
};

export const findAllDictionary = (): Promise<DictionaryItemDto[]> => {
  return DictionaryService.dictionaryControllerFindAllWords();
};

export const addWordToDictionary = (
  item: DictionaryItemDto,
): CancelablePromise<any> => {
  return DictionaryService.dictionaryControllerAddWordToDictionary(item);
};

export const removeWordFromDictionary = (
  request: RemoveDictionaryRequestDto,
): CancelablePromise<number> => {
  return DictionaryService.dictionaryControllerRemoveWordFromDictionary(
    request,
  );
};

export const synhronizeDictionary = (): CancelablePromise<Array<any>> => {
  return DictionaryService.dictionaryControllerSynchronizeDictionary();
};

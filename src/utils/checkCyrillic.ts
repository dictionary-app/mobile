import { DictionaryItemDto } from "../api";

export const checkCyrillic = (text: string) => {
    return /[а-я]/i.test(text);
}

export const checkCyrillicDictionaryItem = (item: DictionaryItemDto) => {
    return checkCyrillic(item.wordValue)
}
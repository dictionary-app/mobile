const gradients = [
  ['#2400fa', '#700bbb'],
  ['#bf0099', '#e00023'],
  ['#fc6b00', '#a80400'],
  ['#EA40EC', '#763DFB'],
  ['#77ff', '#1800df'],
  ['#9b00d5', '#ff8400'],
];

export const getRandomGradient = () => {
  const randomNumber = Math.floor(Math.random() * gradients.length);

  return gradients[randomNumber];
};

import { ITrainingItem } from './../models/TrainingModel';

export const WORD_TRANSLATION_ALIAS = 'wordTranslation'
export const TRANSLATION_WORD_ALIAS = 'translationWord'

export const trainings:ITrainingItem[] = [
    {
        id:1,
        alias: WORD_TRANSLATION_ALIAS,
        title:'Слово-перевод',
        image: require('../../assets/images/training/word-translation.jpeg'),
        navigate: 'WordTranslationGroups',
    },
    {
        id:2,
        alias:TRANSLATION_WORD_ALIAS,
        title:'Перевод-слово',
        image: require('../../assets/images/training/translation-word.jpeg'),
        navigate: 'WordTranslationGroups',
    },
]

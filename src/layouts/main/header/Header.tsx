import React, { FC } from 'react';
import tw from 'tailwind-react-native-classnames';
import { View, Text } from 'react-native';

interface IHeaderProps {
  title?: string;
}
const Header: FC<IHeaderProps> = ({ title = '' }) => {
  return (
    <View
      style={{
        ...tw`w-full pt-8 px-4 bg-gray-50 flex-row justify-center items-center`,
        borderBottomColor: '#E1E1E1',
        borderBottomWidth: 1,
        height: 80,
      }}
    >
      {!!title && <Text style={tw`text-xl`}>{title}</Text>}
    </View>
  );
};

export default Header;

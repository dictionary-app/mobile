import { IFooterItem } from './types';

export const menu: IFooterItem[] = [
  {
    iconName: 'list-circle-outline',
    title: 'Словарь',
    navigate: 'Dictionary',
    isActive: true,
  },
  {
    iconName: 'cube',
    title: 'Наборы',
    navigate: 'Groups',
    isActive: true,
  },
  {
    iconName: 'body-sharp',
    title: 'Тренировки',
    navigate: 'Training',
    isActive: true,
  },
  {
    iconName: 'people',
    title: 'Профиль',
    navigate: 'Profile',
    isActive: true,
  },
];

import { TypeRootStackParamList } from '../../../navigation/types';
import { Ionicons } from '@expo/vector-icons';

export interface IFooterItem {
  iconName: keyof typeof Ionicons.glyphMap;
  title: string;
  navigate: keyof TypeRootStackParamList;
  isActive: boolean;
}

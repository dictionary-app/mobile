import React, { FC } from 'react';
import { Pressable, Text } from 'react-native';
import { IFooterItem } from './types';
import Icon from '@expo/vector-icons/Ionicons';
import tw from 'tailwind-react-native-classnames';
import { TypeRootStackParamList } from '../../../navigation/types';

interface INavItem {
  item: IFooterItem;
  navigate: (screenName: keyof TypeRootStackParamList) => void;
  currentRoute: string;
}

const NavItem: FC<INavItem> = ({ item, navigate, currentRoute }) => {
  const isActive = currentRoute === item.navigate;

  const navigateTo = () => {
    if (item.isActive) navigate(item.navigate);
  };

  const disabled = !item.isActive ? 'text-gray-300' : '';

  return (
    <Pressable
      onPress={navigateTo}
      style={{ ...tw`items-center pb-3 pt-1`, width: '25%' }}
    >
      <Icon
        name={item.iconName}
        style={tw`text-xl ${
          isActive ? 'text-blue-500' : 'text-gray-500'
        } ${disabled}`}
      />
      <Text
        style={{
          ...tw`text-xs ${
            isActive ? 'text-blue-500' : 'text-gray-500'
          } ${disabled}`,
          marginTop: 1,
        }}
      >
        {item.title}
      </Text>
    </Pressable>
  );
};

export default NavItem;

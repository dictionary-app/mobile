import React, { FC } from 'react';
import tw from 'tailwind-react-native-classnames';
import { menu } from './menu';
import NavItem from './NavItem';
import { TypeRootStackParamList } from '../../../navigation/types';
import { View } from 'react-native';

interface IFooter {
  navigate: (screenName: keyof TypeRootStackParamList) => void;
  currentRoute: string;
}

const Footer: FC<IFooter> = ({ navigate, currentRoute }) => {
  return (
    <View
      style={{
        ...tw`flex-row justify-between items-center w-full bg-gray-50 px-3`,
        borderTopColor: '#E1E1E1',
        borderTopWidth: 1,
      }}
    >
      {menu.map((item) => (
        <NavItem
          key={item.title}
          item={item}
          navigate={navigate}
          currentRoute={currentRoute}
        />
      ))}
    </View>
  );
};

export default Footer;

import React, { FC } from 'react';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import InitialLoader from '../../components/initialLoader/InitialLoader';
import { useGlobalStateProvider } from '../../providers/use/useGlobalStateProvider';
import Header from './header/Header';

interface ILayout {
  isScrollView?: boolean;
  isCustomHeader?: boolean;
  headerTitle?: string;
}

const MainLayout: FC<ILayout> = ({
  children,
  isCustomHeader = false,
  headerTitle = '',
}) => {
  const { isLoadingInitialData } = useGlobalStateProvider();
  if (isLoadingInitialData) return <InitialLoader />;

  return (
    <View style={tw`h-full w-full bg-white relative`}>
      {!isCustomHeader && <Header title={headerTitle} />}
      {children}
    </View>
  );
};

export default MainLayout;

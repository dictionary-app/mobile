import React, { FC } from 'react';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import InitialLoader from '../../components/initialLoader/InitialLoader';
import { useGlobalStateProvider } from '../../providers/use/useGlobalStateProvider';

interface ILayout {
  isScrollView?: boolean;
}

const AuthLayout: FC<ILayout> = ({ children }) => {
  const { isLoadingInitialData } = useGlobalStateProvider();
  if (isLoadingInitialData) return <InitialLoader />;

  return (
    <View style={tw`h-full w-full bg-white justify-center items-center`}>
      {children}
    </View>
  );
};

export default AuthLayout;

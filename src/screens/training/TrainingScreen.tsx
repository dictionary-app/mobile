import React, { FC } from 'react';
import MainLayout from '../../layouts/main/MainLayout';
import Container from '../../components/ui/Container';
import TrainingContent from '../../components/trainingSelection/TrainingContent';
import { trainings } from '../../config/training';

const TrainingScreen: FC = () => {
  return (
    <MainLayout headerTitle="Тренировки">
      <Container isScrollable>
         <TrainingContent trainings={trainings}/>
      </Container>
    </MainLayout>
  );
};

export default TrainingScreen;

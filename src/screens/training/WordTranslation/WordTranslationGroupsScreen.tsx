import React, { FC, useEffect } from 'react';
import {
  useIsFocused,
  useNavigation,
  useRoute,
} from '@react-navigation/native';
import Container from '../../../components/ui/Container';
import MainLayout from '../../../layouts/main/MainLayout';
import HeaderBack from '../../../components/HeaderBack';
import { useGroups } from '../../../hooks/groups/useGroups';
import Loader from '../../../components/ui/Loader';
import TrainingGroupsContent from '../../../components/trainingSelection/groups/TrainingGroupsContent';

const WordTranslationGroupsScreen: FC = () => {
  const route = useRoute();
  const { navigate } = useNavigation();
  // @ts-ignore
  const trainingAlias = route.params?.trainingAlias;

  const isFocused = useIsFocused();
  const { groups, getGroups, isLoadingGroups } = useGroups();

  const clickHandler = (groupId: number | string) => {
    navigate('WordTranslationTraining', { groupId, trainingAlias });
  };

  useEffect(() => {
    if (isFocused) getGroups();
  }, [isFocused]);

  return (
    <MainLayout isCustomHeader>
      <HeaderBack
        onBack={() => navigate('Training')}
        title={'Выберите набор'}
      />
      <Container isScrollable>
        {!isLoadingGroups ? (
          <Loader />
        ) : (
          <TrainingGroupsContent groups={groups} clickHandler={clickHandler} />
        )}
      </Container>
    </MainLayout>
  );
};

export default WordTranslationGroupsScreen;

import React, { FC } from 'react';
import HeaderBack from '../../../components/HeaderBack';
import TranslationWordContent from '../../../components/trainings/TranslationWordTraining/TranslationWordContent';
import WordTranslationContent from '../../../components/trainings/WordTranslationTraining/WordTranslationContent';
import Container from '../../../components/ui/Container';
import { WORD_TRANSLATION_ALIAS } from '../../../config/training';
import { useWordTranslationTraining } from '../../../hooks/training/useWordTranslationTraining';
import MainLayout from '../../../layouts/main/MainLayout';

const WordTranslationTrainingScreen: FC = () => {
  const { backClickHandler, training, words } = useWordTranslationTraining();

  return (
    <MainLayout isCustomHeader>
      <HeaderBack onBack={backClickHandler} title={training?.title || ''} />
      {words && (
        <Container>
          {training?.alias === WORD_TRANSLATION_ALIAS ? (
            <WordTranslationContent items={words} />
          ) : (
            <TranslationWordContent items={words} />
          )}
        </Container>
      )}
    </MainLayout>
  );
};

export default WordTranslationTrainingScreen;

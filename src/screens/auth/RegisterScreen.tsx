import React, { FC, useState } from 'react';
import { View, Text, Pressable, Alert } from 'react-native';
import AuthLayout from '../../layouts/auth/AuthLayout';
import tw from 'tailwind-react-native-classnames';
import Field from '../../components/ui/Field';
import Button from '../../components/ui/Button';
import { useNavigation } from '@react-navigation/native';
import { UserDto } from '../../../generated/api';
import { useGlobalStateProvider } from '../../providers/use/useGlobalStateProvider';
import { registerApi } from '../../api/auth';

interface IUser extends UserDto {
  password: string;
}

const RegisterScreen: FC = () => {
  const [localUser,setLocalUser] = useState<IUser>({
    name: '',
    login: '',
    email: '',
    password: '',
  });

  const { navigate } = useNavigation();

  const { setUser } = useGlobalStateProvider()

  const registerHandler = async () => {
    try {
      const { user } = await registerApi(localUser)
      setUser(user)
      navigate('Dictionary')
    } catch(e: any) {
      Alert.alert(e.body.message)
    }
  };
  return (
    <AuthLayout>
      <View style={tw`w-5/6`}>
        <Text style={tw`text-center text-gray-800 text-2xl font-bold mb-2`}>
          {'Регистрация'}
        </Text>
        <Field
          val={localUser.name}
          placeholder="Имя"
          onChange={(name) =>
            setLocalUser({
              ...localUser,
              name,
            })
          }
        />
        <Field
          val={localUser.email}
          placeholder="E-mail"
          onChange={(email) =>
            setLocalUser({
              ...localUser,
              email,
            })
          }
        />
        <Field
          val={localUser.login}
          placeholder="Логин"
          onChange={(login) =>
            setLocalUser({
              ...localUser,
              login,
            })
          }
        />
        <Field
          val={localUser.password}
          placeholder="Пароль"
          onChange={(password) =>
            setLocalUser({
              ...localUser,
              password,
            })
          }
          isSecure={true}
        />
        <Button onPress={registerHandler} title="Зарегистрироваться" />

        <Pressable onPress={() => navigate('Auth')}>
          <Text style={tw`text-right text-sm text-gray-800 opacity-30`}>
            {'Авторизация'}
          </Text>
        </Pressable>
      </View>
    </AuthLayout>
  );
};

export default RegisterScreen;

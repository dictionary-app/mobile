import React, { FC, useState } from 'react';
import { View, Text, Pressable, Alert } from 'react-native';
import AuthLayout from '../../layouts/auth/AuthLayout';
import tw from 'tailwind-react-native-classnames';
import { useNavigation } from '@react-navigation/native';
import Field from '../../components/ui/Field';
import Button from '../../components/ui/Button';
import { LoginRequestDto } from '../../../generated/api';
import { loginApi } from '../../api/auth';
import { useGlobalStateProvider } from '../../providers/use/useGlobalStateProvider';

const AuthScreen: FC = () => {
  const [localUser, setLocalUser] = useState<LoginRequestDto>({
    email: '',
    password: '',
  });

  const { setUser } = useGlobalStateProvider();

  const [isLoading, setIsLoading] = useState(false);

  const { navigate } = useNavigation();

  const authHandler = async () => {
    try {
      setIsLoading(true);
      const { user } = await loginApi(localUser);
      setUser(user);
      navigate('Dictionary');
    } catch (e: any) {
      Alert.alert(e.body.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <AuthLayout>
      <View style={tw`w-5/6`}>
        <Text style={tw`text-center text-gray-800 text-2xl font-bold mb-2`}>
          {'Авторизация'}
        </Text>
        <Field
          val={localUser.email}
          placeholder="E-mail"
          onChange={(email) =>
            setLocalUser({
              ...localUser,
              email,
            })
          }
        />
        <Field
          val={localUser.password}
          placeholder="Пароль"
          onChange={(password) =>
            setLocalUser({
              ...localUser,
              password,
            })
          }
          isSecure={true}
        />
        <Button isLoading={isLoading} onPress={authHandler} title="Войти" />
      </View>
    </AuthLayout>
  );
};

export default AuthScreen;

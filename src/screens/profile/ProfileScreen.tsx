import React, { FC, useState } from 'react';
import { View, Text } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import MainLayout from '../../layouts/main/MainLayout';
import Container from '../../components/ui/Container';
import Loader from '../../components/ui/Loader';
import Field from '../../components/ui/Field';
import Button from '../../components/ui/Button';
import { useGlobalStateProvider } from '../../providers/use/useGlobalStateProvider';
import { UserEntity } from '../../api';
import { updateUser } from '../../api/users';
import { logoutApi } from '../../api/auth';
import { useNavigation } from '@react-navigation/native';

const ProfileScreen: FC = () => {
  const { navigate } = useNavigation()
  const { user, setUser } = useGlobalStateProvider();

  const [localUser, setLocalUser] = useState<UserEntity>(user as UserEntity);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const updateProfile = async () => {
    const responseUser = await updateUser(localUser.id, {
      name:localUser.name,
      email:localUser.email
    })
    setIsSuccess(true)
    setTimeout(function() {
      setIsSuccess(false)
    },2000)
    setUser(responseUser)
  };

  const logoutHandler = async () => {
    await logoutApi()
    await setUser(null)
    navigate('Auth')
  }

  return (
    <MainLayout headerTitle="Профиль">
      <Container>
        {isSuccess && (
          <View style={tw`bg-green-500 p-3 py-2 rounded-md mb-4`}>
            <Text style={tw`text-white text-center`}>
              Профиль успешно обновлен
            </Text>
          </View>
        )}
        {isLoading ? (
          <Loader />
        ) : (
          <>
            <Field
              onChange={(name) =>
                setLocalUser({
                  ...localUser,
                  name,
                })
              }
              val={localUser.name}
              placeholder="Введите имя"
            />
            <Field
              onChange={(email) =>
                setLocalUser({
                  ...localUser,
                  email,
                })
              }
              val={localUser.email}
              placeholder="Введите почту"
            />
            <Button onPress={updateProfile} title="Обновить профиль" />
            <Button
              onPress={logoutHandler}
              title="Выйти"
              colors={['bg-gray-200', '#D6D8DB']}
            />
          </>
        )}
      </Container>
    </MainLayout>
  );
};

export default ProfileScreen;

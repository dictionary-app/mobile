import React, { FC } from 'react';
import MainLayout from '../../layouts/main/MainLayout';
import Container from '../../components/ui/Container';
import HeaderBack from '../../components/HeaderBack';
import GroupsItemTitle from '../../components/groups/item/GroupsItemTitle';
import AddButton from '../../components/ui/AddButton';
import { useGroupSearchItem } from '../../hooks/groups/useGroupSearchItem';
import { useGroupItem } from '../../hooks/groups/useGroupItem';
import GroupsItemsContainer from '../../components/groups/item/MainScreen/GroupsItemsContainer';
import GroupsAddItemsContainer from '../../components/groups/item/AddScreen/GroupsAddItemsContainer';
import Loader from '../../components/ui/Loader';

const GroupsItemScreen: FC = () => {
  const {
    search,
    title,
    isOpenSearch,
    filteredWords,
    words,
    isLoadingGroup,
    setTitle,
    onPressBackButton,
    setSearch,
    addButtonHandler,
    headerTitle,
    iconNameAddButton,
    addRemoveWordItem,
    removeWord,
    onUpdateHandler,
    id,
  } = useGroupItem();

  const { dictionarySearch, setDictionarySearch, dictionaryFilteredItems } =
    useGroupSearchItem();

  const content = !isOpenSearch ? (
    <>
      <GroupsItemTitle
        title={title}
        setTitle={setTitle}
        onUpdateHandler={onUpdateHandler}
      />
      <GroupsItemsContainer
        search={search}
        setSearch={setSearch}
        filteredItems={filteredWords}
        items={words}
        remove={removeWord}
        isLoading={isLoadingGroup}
      />
    </>
  ) : (
    <GroupsAddItemsContainer
      groupId={id}
      items={dictionaryFilteredItems}
      search={dictionarySearch}
      setSearch={setDictionarySearch}
      words={words}
      onPressItem={addRemoveWordItem}
    />
  );

  return (
    <MainLayout isCustomHeader={true}>
      <HeaderBack onBack={onPressBackButton} title={headerTitle()}></HeaderBack>
      <Container>{isLoadingGroup ? <Loader /> : content}</Container>
      <AddButton iconName={iconNameAddButton()} onPress={addButtonHandler} />
    </MainLayout>
  );
};

export default GroupsItemScreen;

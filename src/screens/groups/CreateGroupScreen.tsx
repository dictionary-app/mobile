import React, { FC } from 'react';
import MainLayout from '../../layouts/main/MainLayout';
import Container from '../../components/ui/Container';
import HeaderBack from '../../components/HeaderBack';
import { useGroupItem } from '../../hooks/groups/useGroupItem';
import MaterialIcon from '@expo/vector-icons/MaterialIcons';

import tw from 'tailwind-react-native-classnames';
import { View, Pressable, TextInput } from 'react-native';
import Button from '../../components/ui/Button';
import { useNavigation } from '@react-navigation/native';

const CreateGroupScreen: FC = () => {
  const { navigate } = useNavigation();

  const { title, onPressBackButton, headerTitle, setTitle, onCreateHandler } =
    useGroupItem();

  const changeTitleHandler = (text: string) => {
    setTitle(text);
  };

  return (
    <MainLayout isCustomHeader={true}>
      <HeaderBack onBack={onPressBackButton} title={headerTitle()}></HeaderBack>
      <Container>
        <View
          style={{
            ...tw`flex-row w-full bg-gray-100 h-10 items-center justify-between rounded-md`,
          }}
        >
          <TextInput
            placeholder="Название набора"
            onChangeText={changeTitleHandler}
            value={title}
            autoCapitalize="none"
            style={{ ...tw`px-3 py-2  w-5/6` }}
          />
          {
            <Pressable onPress={() => changeTitleHandler('')}>
              <MaterialIcon
                name="highlight-remove"
                size={20}
                style={{ ...tw`w-8 text-gray-400`, height: 20 }}
              />
            </Pressable>
          }
        </View>
        <View
          style={{
            ...tw`flex-row w-full items-center justify-between`,
          }}
        >
          <View style={{ ...tw`pr-3 w-1/2` }}>
            <Button
              title="Отмена"
              style={{ ...tw`w-full` }}
              onPress={() => navigate('Groups')}
            />
          </View>
          <View style={{ ...tw`pl-3 w-1/2` }}>
            <Button
              title="Дальше"
              style={{ ...tw`w-full` }}
              onPress={() => onCreateHandler()}
            />
          </View>
        </View>
      </Container>
    </MainLayout>
  );
};

export default CreateGroupScreen;

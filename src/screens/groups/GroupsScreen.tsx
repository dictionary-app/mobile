import React, { FC, useEffect } from 'react';
import MainLayout from '../../layouts/main/MainLayout';

import AddButton from '../../components/ui/AddButton';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import Container from '../../components/ui/Container';
import { useGroups } from '../../hooks/groups/useGroups';
import GroupsContent from '../../components/groups/items/GroupsContent';
import Loader from '../../components/ui/Loader';

const GroupsScreen: FC = () => {
  const { navigate } = useNavigation();
  const isFocused = useIsFocused();
  const { groups, getGroups, onRemove, isLoadingGroups } = useGroups();

  const clickHandler = (id: number) => {
    navigate('GroupItem', { id });
  };

  useEffect(() => {
    if (isFocused) getGroups();
  }, [isFocused]);

  return (
    <MainLayout headerTitle="Наборы слов">
      <Container isScrollable>
        {!isLoadingGroups ? (
          <Loader />
        ) : (
          <GroupsContent
            groups={groups}
            onRemove={onRemove}
            clickHandler={clickHandler}
          />
        )}
      </Container>
      {!(!groups || !groups.length) && (
        <AddButton onPress={() => navigate('CreateGroup')} />
      )}
    </MainLayout>
  );
};

export default GroupsScreen;

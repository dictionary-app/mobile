import React, { FC, useEffect } from 'react';
import DictionaryHeader from '../../components/dictionary/DictionaryHeader';
import { useDictionaryProvider } from '../../providers/use/useDictionaryProvider';
import MainLayout from '../../layouts/main/MainLayout';
import Container from '../../components/ui/Container';
import DictionaryContent from '../../components/dictionary/DictionaryContent';
import Loader from '../../components/ui/Loader';

const DictionaryScreen: FC = () => {
  const { getDictionary, filteredItems, isDictionatyLoading } =
    useDictionaryProvider();
    
  useEffect(() => {
    getDictionary();
  }, []);

  return (
    <MainLayout headerTitle="Словарь">
      <Container>
        <DictionaryHeader />
        {isDictionatyLoading ? (
          <Loader style={{ height:'75%' }} />
        ) : (
          <DictionaryContent filteredItems={filteredItems} />
        )}
      </Container>
    </MainLayout>
  );
};

export default DictionaryScreen;

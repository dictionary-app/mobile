import React, { FC, useState } from 'react';
import { View } from 'react-native';
import MainLayout from '../../layouts/main/MainLayout';
import tw from 'tailwind-react-native-classnames';
import { useSearch } from '../../hooks/search/useSearch';
import SearchItem from '../../components/search/SearchItem';
import SearchHeader from '../../components/search/SearchHeader';
import Button from '../../components/ui/Button';
import { useDictionaryProvider } from '../../providers/use/useDictionaryProvider';
import { ResponseStatus } from '../../api/types';
import { useNavigation, useRoute } from '@react-navigation/native';
import Container from '../../components/ui/Container';
import { checkCyrillicDictionaryItem } from '../../utils/checkCyrillic';
import { addWordToGroup } from '../../api/groups';

const SearchScreen: FC = () => {
  const { navigate } = useNavigation();

  const { addDictionaryItem, setSearch: setSearchDictionary } =
    useDictionaryProvider();
  const {
    search,
    searchHandler,
    clearHandler,
    items,
    setSearch,
    reverseSearchWord,
    setItemCandidate,
    itemCandidate,
  } = useSearch();

  const route = useRoute();

  const [isLoading, setIsLoading] = useState(false);

  // @ts-ignore
  const groupId = route.params?.groupId;

  const afterAddingHandler = async (dictionaryId: number | undefined) => {
    if (dictionaryId) {
      setSearchDictionary('');
      if (groupId) {
        await addWordToGroup({
          dictionaryId,
          groupId,
        });
        navigate('GroupItem', { id: groupId });
      } else {
        navigate('Dictionary');
      }
    }
  };

  const addItem = async () => {
    if (!itemCandidate) return;
    setIsLoading(true);
    try {
      if (checkCyrillicDictionaryItem(itemCandidate)) {
        const candidate = await reverseSearchWord(itemCandidate);
        if (candidate) {
          const id = await addDictionaryItem(candidate);
          afterAddingHandler(id);
        }
      }
      if (itemCandidate) {
        const id = await addDictionaryItem(itemCandidate);
        afterAddingHandler(id);
      }
    } catch (err) {
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <MainLayout headerTitle="Поиск слов">
      <Container>
        <SearchHeader
          search={search}
          searchHandler={searchHandler}
          clearHandler={clearHandler}
          setSearch={setSearch}
        />
        <View style={tw`pt-4`}>
          {items.map((item) => (
            <SearchItem
              key={item.twordId}
              item={item}
              selectedItem={itemCandidate}
              selectItem={setItemCandidate}
            />
          ))}
        </View>
        {itemCandidate && (
          <Button
            onPress={addItem}
            title="Добавить в словарь"
            isLoading={isLoading}
          />
        )}
      </Container>
    </MainLayout>
  );
};

export default SearchScreen;

import { ImageSourcePropType } from "react-native";
import { TypeRootStackParamList } from "../navigation/types";

export interface ITrainingItem {
    id: number;
    alias: string,
    title: string,
    image: ImageSourcePropType,
    navigate: keyof TypeRootStackParamList;
}

  
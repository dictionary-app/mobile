import { Dimensions } from "react-native";

const { height } = Dimensions.get('screen')

export const CARD = {
    HEIGHT: height - 400,
    BORDER_RADIUS: 20
}

export const ACTION_OFFSET = 100;
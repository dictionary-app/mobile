import React from 'react';
import tw from 'tailwind-react-native-classnames';
import { View } from 'react-native';
import Loader from '../ui/Loader';

const InitialLoader = () => {
  return (
    <View style={tw`h-full w-full justify-center items-center flex-row`}>
      <Loader />
    </View>
  )
};

export default InitialLoader;

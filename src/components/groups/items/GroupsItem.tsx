import React, { FC } from 'react';
import { Pressable, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import tw from 'tailwind-react-native-classnames';
import { getRandomGradient } from '../../../utils/getRandomGradient';
import { GroupEntity } from '../../../api';

interface IGroupItemProps {
  group: GroupEntity;
  onRemove: (group: GroupEntity) => Promise<void>;
  clickHandler: (id: number) => void;
}

const GroupsItem: FC<IGroupItemProps> = ({ group, onRemove, clickHandler }) => {
  return (
    <Pressable
      onPress={() => clickHandler(group.id)}
      onLongPress={() => onRemove(group)}
      style={{ ...tw`rounded-md overflow-hidden mx-2 w-2/5 mb-6`, height: 120 }}
    >
      <LinearGradient
        colors={getRandomGradient()}
        style={{ ...tw`w-full h-full items-center justify-center opacity-80` }}
      >
        <Text style={tw`text-white text-center font-bold px-4`}>
          {group.title}
        </Text>
      </LinearGradient>
    </Pressable>
  );
};

export default GroupsItem;

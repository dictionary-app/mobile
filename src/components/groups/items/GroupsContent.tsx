import React, { FC } from 'react';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import { GroupEntity } from '../../../api';
import GroupsItem from './GroupsItem';
import GroupsNotFound from './GroupsNotFound';

interface IGroupsContentProps {
  groups: GroupEntity[];
  onRemove: (group: GroupEntity) => Promise<void>;
  clickHandler: (id: number) => void;
}

const GroupsContent: FC<IGroupsContentProps> = ({
  groups,
  onRemove,
  clickHandler,
}) => {
  return (
    <View style={tw`flex-row flex-wrap justify-center`}>
      {groups &&
        groups
          .reverse()
          .map((group) => (
            <GroupsItem
              key={group.id}
              group={group}
              onRemove={onRemove}
              clickHandler={clickHandler}
            />
          ))}
      {(!groups || !groups.length) && <GroupsNotFound />}
    </View>
  );
};

export default GroupsContent;

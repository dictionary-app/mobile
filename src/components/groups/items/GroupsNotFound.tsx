import { useNavigation } from '@react-navigation/native';
import React, { FC } from 'react';
import { Text, View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import Button from '../../ui/Button';

const GroupsNotFound: FC = () => {
  const { navigate } = useNavigation();
  return (
    <View style={tw`pt-4 w-full`}>
      <Text style={tw`text-center text-gray-500 pb-2`}>
        У вас нет сохранённых наборов
      </Text>
      <Button title="Добавить" onPress={() => navigate('CreateGroup')} />
    </View>
  );
};

export default GroupsNotFound;

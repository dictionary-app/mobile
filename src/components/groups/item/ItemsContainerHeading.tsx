import React, { FC } from 'react';
import { Text } from 'react-native';
import tw from 'tailwind-react-native-classnames';

const ItemsContainerHeading: FC<{ text: string; style?: any }> = ({ text, style }) => {
  return (
    <Text style={{ ...tw`text-lg text-gray-800 pb-2 pl-2 text-center`, ...style }}>
      {text}
    </Text>
  );
};

export default ItemsContainerHeading;

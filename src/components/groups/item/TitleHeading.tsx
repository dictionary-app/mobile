import React, { FC } from 'react';
import { Text } from 'react-native';
import tw from 'tailwind-react-native-classnames';

const TitleHeading: FC<{ text: string; style?: any }> = ({ text, style }) => {
  return (
    <>
    <Text style={tw`text-sm text-gray-500 text-center`}>Название набора</Text>
      <Text
        style={{
          ...tw`text-xl text-center font-bold text-gray-800`,
          ...style,
        }}
      >
        {text}
      </Text>
    </>
  );
};

export default TitleHeading;

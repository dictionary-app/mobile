import React, { Dispatch, FC, SetStateAction } from 'react';
import { Text, Pressable, Image, View } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';

import tw from 'tailwind-react-native-classnames';
import { useSound } from '../../../../hooks/useSound';
import { DictionaryItemDto } from '../../../../api';

interface IGroupSearchItem {
  item: DictionaryItemDto;
  onPress: (item: DictionaryItemDto) => void;
  isAdded: boolean
}

const GroupSearchItem: FC<IGroupSearchItem> = ({ item, onPress, isAdded }) => {
  const { playSound, isPlaying } = useSound(item.pronunciation);

  return (
    <Pressable
      onPress={() => onPress(item)}
      style={tw`px-3 py-3 border-b border-gray-300 border-solid flex-row items-center justify-between ${ isAdded ? 'bg-blue-50' : ''}`}
    >
      <View style={tw`flex-row items-center`}>
        {!!item.picture && (
          <Image
            source={{ uri: item.picture }}
            style={{ ...tw`rounded-lg w-10 h-10 mr-4` }}
          />
        )}
        <View style={tw`w-2/3`}>
          <Text style={{ ...tw`text-blue-600`, lineHeight: 20 }}>
            {item.wordValue}
          </Text>
          <Text style={{ lineHeight: 20 }}>{item.translateValue}</Text>
        </View>
      </View>

      <View style={tw`flex-row items-center`}>
        {!!item.pronunciation && (
          <Pressable onPress={playSound}>
            <Ionicons
              size={26}
              name="volume-medium"
              style={tw`${isPlaying ? 'text-blue-400' : 'text-gray-600'}`}
            />
          </Pressable>
        )}
      </View>
    </Pressable>
  );
};

export default GroupSearchItem;

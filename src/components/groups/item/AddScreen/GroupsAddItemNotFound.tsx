import { useNavigation } from '@react-navigation/native';
import React, { FC } from 'react';
import { View, Text } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import Button from '../../../ui/Button';

interface IGroupsAddItemNotFound {
  search: string;
  groupId: number;
}
const GroupsAddItemNotFound: FC<IGroupsAddItemNotFound> = ({
  search,
  groupId,
}) => {
  const { navigate } = useNavigation();
  return (
    <View style={tw`pt-4`}>
      <Text style={tw`text-center text-gray-500 pb-2`}>
        В вашем словаре ничего не найдено
      </Text>
      <Button
        title="Добавить"
        onPress={() => navigate('Search', { search, groupId })}
      />
    </View>
  );
};

export default GroupsAddItemNotFound;

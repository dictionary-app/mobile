import React, { Dispatch, FC, SetStateAction } from 'react';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import GroupsSearch from '../MainScreen/GroupsSearch';
import GroupSearchItem from './GroupSearchItem';
import ContentScrollView from '../../../ui/ContentScrollView';
import GroupsAddItemNotFound from './GroupsAddItemNotFound';
import { DictionaryItemDto } from '../../../../api';

interface IGroupsSearchProps {
  style?: any;
  items: DictionaryItemDto[];
  search: string;
  groupId: number;
  setSearch: Dispatch<SetStateAction<string>>;
  onPressItem: (item: DictionaryItemDto) => void;
  words: DictionaryItemDto[];
}

const GroupsAddItemsContainer: FC<IGroupsSearchProps> = ({
  items,
  search,
  setSearch,
  style,
  onPressItem,
  groupId,
  words,
}) => {
  const isAdded = (item: DictionaryItemDto) =>
    !!words.find((c) => c.localId === item.localId);

  return (
    <View style={{ ...style }}>
      <GroupsSearch search={search} setSearch={setSearch} />
      {items && items.length ? (
        <View style={tw`pt-4`}>
          <ContentScrollView>
            {items &&
              items.map((item) => (
                <GroupSearchItem
                  key={item.localId}
                  item={item}
                  isAdded={isAdded(item)}
                  onPress={onPressItem}
                />
              ))}
          </ContentScrollView>
        </View>
      ) : (
        <GroupsAddItemNotFound search={search} groupId={groupId} />
      )}
    </View>
  );
};

export default GroupsAddItemsContainer;

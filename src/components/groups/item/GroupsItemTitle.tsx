import React, { Dispatch, FC, SetStateAction, useState } from 'react';
import { Pressable, TextInput, View } from 'react-native';
import MaterialIcon from '@expo/vector-icons/MaterialIcons';
import Icon from '@expo/vector-icons/Entypo';
import tw from 'tailwind-react-native-classnames';
import TitleHeading from './TitleHeading';

interface IGroupsItemTitle {
  title: string;
  setTitle: Dispatch<SetStateAction<string>>;
  onUpdateHandler: () => void;
  style?: any;
}

const GroupsItemTitle: FC<IGroupsItemTitle> = ({
  style,
  title,
  setTitle,
  onUpdateHandler,
}) => {
  const [isOpenHeadingForm, setIsOpenHeadingForm] = useState(false);
  const [isThouchedTitle, setIsTouchedTitle] = useState(false);

  const changeTitleHandler = (text: string) => {
    if (!isThouchedTitle) setIsTouchedTitle(true);
    setTitle(text);
  };

  const saveHandler = async () => {
    onUpdateHandler();
    setIsTouchedTitle(false);
    if (title.length) setIsOpenHeadingForm(false);
  };

  return (
    <View style={tw`pb-6`}>
      {isOpenHeadingForm || !title.length || isThouchedTitle ? (
        <View style={{ ...tw`flex-row items-center justify-between` }}>
          <View
            style={{
              ...tw`flex-row w-5/6 bg-gray-100 h-10 items-center justify-between rounded-md`,
              ...style,
            }}
          >
            <TextInput
              placeholder="Название набора"
              onChangeText={changeTitleHandler}
              value={title}
              autoCapitalize="none"
              style={{ ...tw`px-3 py-2  w-5/6` }}
            />
            {
              <Pressable onPress={() => changeTitleHandler('')}>
                <MaterialIcon
                  name="highlight-remove"
                  size={20}
                  style={{ ...tw`w-8 text-gray-400`, height: 20 }}
                />
              </Pressable>
            }
          </View>
          <Pressable
            style={tw`w-12 h-10 bg-yellow-300 items-center justify-center rounded-md`}
            onPress={saveHandler}
          >
            <Icon name="check" size={20} style={tw`text-gray-600`} />
          </Pressable>
        </View>
      ) : (
        <Pressable onPress={() => setIsOpenHeadingForm(true)}>
          <TitleHeading text={title} />
        </Pressable>
      )}
    </View>
  );
};

export default GroupsItemTitle;

import React, { FC } from 'react';
import { View, Text } from 'react-native';
import tw from 'tailwind-react-native-classnames';

const NotFoundWrapper: FC = () => {
  return (
    <View style={tw`pt-4`}>
      <Text style={tw`text-center text-gray-900 text-lg pb-2`}>
        У вас не добавлены слова в набор
      </Text>
      <Text style={tw`text-center text-gray-500 pb-2`}>
        Добавьте их из словаря, нажав кнопку с плюсом
      </Text>
    </View>
  );
};

export default NotFoundWrapper;

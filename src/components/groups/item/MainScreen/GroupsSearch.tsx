import React, { Dispatch, FC, SetStateAction } from 'react';
import { Pressable, View, TextInput } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';
import MaterialIcon from '@expo/vector-icons/MaterialIcons';
import tw from 'tailwind-react-native-classnames';

interface IGroupsSearchProps {
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
  style?: any;
}

const GroupsSearch: FC<IGroupsSearchProps> = ({
  search,
  setSearch,
  style,
}) => {
  return (
    <View style={{ ...tw`flex-row items-center justify-between`, ...style }}>
      <View
        style={tw`flex-row w-full h-10 bg-gray-100 items-center justify-between rounded-md`}
      >
        <TextInput
          placeholder="Поиск"
          onChangeText={setSearch}
          value={search}
          autoCapitalize="none"
          style={{ ...tw`px-3 py-2  w-5/6` }}
        />
        {!!search.length && (
          <Pressable onPress={() => setSearch('')}>
            <MaterialIcon
              name="highlight-remove"
              size={20}
              style={{ ...tw`w-8 text-gray-400`, height: 20 }}
            />
          </Pressable>
        )}
      </View>
    </View>
  );
};

export default GroupsSearch;

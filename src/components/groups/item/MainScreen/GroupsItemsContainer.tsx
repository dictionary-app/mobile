import React, { Dispatch, FC, SetStateAction } from 'react';
import { View, ScrollView, Text } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import GroupsSearch from './GroupsSearch';
import GroupDictionaryItem from './GroupDictionaryItem';
import ContentScrollView from '../../../ui/ContentScrollView';
import GroupDictionaryNotFound from './GroupDictionaryNotFound';
import { DictionaryItemDto } from '../../../../api';

interface IGroupsSearchProps {
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
  items: DictionaryItemDto[];
  filteredItems: DictionaryItemDto[];
  style?: any;
  remove: (item: DictionaryItemDto) => void;
  isLoading: boolean
}

const GroupsItemsContainer: FC<IGroupsSearchProps> = ({
  items,
  filteredItems,
  setSearch,
  remove,
  search,
  style,
  isLoading
}) => {

  const getNotFound = () => {
    return !isLoading && <GroupDictionaryNotFound />
  }
  return (
    <View style={{ ...style }}>
      {items && items.length ? (
        <>
          <GroupsSearch search={search} setSearch={setSearch} />
          <View style={tw`pt-4`}>
            <ContentScrollView>
              {filteredItems &&
                filteredItems.map((item) => (
                  <ScrollView key={item.localId}>
                    <GroupDictionaryItem item={item} remove={remove} />
                  </ScrollView>
                ))}
            </ContentScrollView>
          </View>
        </>
      ) : (
        getNotFound()
      )}
    </View>
  );
};

export default GroupsItemsContainer;

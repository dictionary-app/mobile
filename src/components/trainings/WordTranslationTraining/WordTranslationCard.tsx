import React, { Dispatch, FC, SetStateAction, useCallback } from 'react';
import { View, Text, Animated, Image, Pressable } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';
import tw from 'tailwind-react-native-classnames';
import { BOX_SHADOW } from '../../../styles';
import WordTranslationSnackBar from '../../ui/training/WordTranslationSnackbar';
import WordTranslationInput from '../../ui/training/WordTranslationInput';
import Button from '../../ui/Button';
import { useWordTranslationCard } from '../../../hooks/training/useWordTranslationCard';
import { DictionaryItemDto } from '../../../api';

interface IWordTranslationCardProps {
  style?: any;
  item: DictionaryItemDto;
  isFirst: boolean;
  setLocalItems: Dispatch<SetStateAction<DictionaryItemDto[]>>;
  setAnswers: Dispatch<SetStateAction<number>>;
  setTrueAnswers: Dispatch<SetStateAction<number>>;
}

const WordTranslationCard: FC<IWordTranslationCardProps> = ({
  item,
  style,
  isFirst,
  setLocalItems,
  setAnswers,
  setTrueAnswers,
}) => {
  const {
    panResponder,
    animatedCardStyle,
    dontKnowOpacity,
    knowOpacity,
    isOpenKeyboard,
    isFalseAnswer,
    isPlaying,
    translateValue,
    isValidInput,
    checkWordTranslateHandler,
    nextHandler,
    dontKnowHandler,
    setTranslateValue,
    playSound,
    setOnLongMove,
  } = useWordTranslationCard(item, setLocalItems, setAnswers, setTrueAnswers);

  const dragHandlers = isFirst ? panResponder.panHandlers : null;

  const getAnimagetCardStyle = () => {
    return isFirst ? animatedCardStyle : null;
  };

  const renderSnackbar = useCallback(() => {
    return (
      <>
        <WordTranslationSnackBar
          title={'Не знаю'}
          opacity={dontKnowOpacity}
          borderColor="red"
        />
        <WordTranslationSnackBar
          title={'Знаю'}
          opacity={knowOpacity}
          borderColor="green"
        />
      </>
    );
  }, []);

  return (
    <Animated.View
      style={{
        ...tw`w-full h-full rounded-2xl bg-gray-50 absolute`,
        height: isOpenKeyboard ? 270 : 370,
        ...BOX_SHADOW,
        ...getAnimagetCardStyle(),
        ...style,
      }}
      {...dragHandlers}
    >
      <Pressable
        onLongPress={() => setOnLongMove(true)}
        onPressOut={() => setOnLongMove(false)}
        style={tw`relative w-full h-full flex-col justify-between items-center p-8`}
        delayLongPress={200}
      >
        {!!item.picture && !isOpenKeyboard && (
          <Image
            source={{ uri: item.picture }}
            style={{ ...tw`rounded-lg w-24 h-24` }}
          />
        )}

        <View
          style={{
            ...tw`flex-col items-center pt-1`,
            paddingTop: isOpenKeyboard ? 0 : 8,
          }}
        >
          {!!item.pronunciation && isFalseAnswer && (
            <Pressable onPress={playSound}>
              <Icon
                size={34}
                name="volume-medium"
                style={tw`${isPlaying ? 'text-blue-400' : 'text-gray-600'}`}
              />
            </Pressable>
          )}
          <Text style={tw`font-medium text-gray-600 text-base pt-1`}>
            {item.translateValue}
          </Text>
          <Text
            style={tw`font-medium text-green-500 text-base pt-1 pb-2 ${
              isFalseAnswer ? 'opacity-100' : 'opacity-0'
            } `}
          >
            {item.wordValue}
          </Text>
        </View>
        <View style={tw`flex-col items-center w-full`}>
          <WordTranslationInput
            value={translateValue}
            setValue={setTranslateValue}
            isValid={isValidInput}
          />
          {isFalseAnswer ? (
            <Button
              style={tw`w-full`}
              colors={['bg-red-500', '#c22323']}
              textStyle="text-white font-semibold"
              onPress={nextHandler}
              title={'Дальше'}
            />
          ) : (
            <View style={tw`flex-row items-center justify-between w-full`}>
              <Button
                style={tw`flex-1 mr-4`}
                colors={['bg-red-500', '#c22323']}
                textStyle="text-white font-semibold"
                onPress={dontKnowHandler}
                title={'Не знаю'}
              />
              <Button
                style={tw`flex-1 ml-4`}
                colors={['bg-green-500', '#23c245']}
                textStyle="text-white font-semibold"
                onPress={checkWordTranslateHandler}
                title={'Проверить'}
              />
            </View>
          )}
        </View>

        {isFirst && renderSnackbar()}
      </Pressable>
    </Animated.View>
  );
};

export default WordTranslationCard;

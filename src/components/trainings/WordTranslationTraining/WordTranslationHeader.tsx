import React, { FC } from 'react';
import { View, Text } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import Container from '../../ui/Container';
import LinearProgressBar from '../../ui/LinearProgressBar';

interface IWordTranslationHeaderProps {
  style?: any;
  trueAnswersPercent: number
}

const WordTranslationHeader: FC<IWordTranslationHeaderProps> = ({ style={}, trueAnswersPercent }) => {
  return (
    <View
      style={{
        ...tw`w-full rounded-xl border border-gray-200 bg-gray-50 border-solid px-4 py-2`,
        height: 70,
        ...style,
      }}
    >
      <Text style={tw`text-center text-base text-gray-700 font-semibold`}>
        <Text style={tw`text-green-500 text-base font-semibold`}>{ `${trueAnswersPercent}%` } </Text> правильных ответов
      </Text>
      <LinearProgressBar style={tw`px-4 pt-2`} percent={trueAnswersPercent}/>
    </View>
  );
};

export default WordTranslationHeader;

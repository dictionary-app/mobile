import React, { FC } from 'react';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import { DictionaryItemDto } from '../../../api';
import { useKeyboard } from '../../../hooks/instruments/useKeyboard';
import { useWordTranslationContent } from '../../../hooks/training/useWordTranslationContent';
import WordTranslationCard from './WordTranslationCard';
import WordTranslationHeader from './WordTranslationHeader';

interface IWordTranslationContentProps {
  items: DictionaryItemDto[];
}

const WordTranslationContent: FC<IWordTranslationContentProps> = ({
  items,
}) => {
  const {
    localItems,
    setLocalItems,
    setAnswers,
    setTrueAnswers,
    trueAnswersPercent,
  } = useWordTranslationContent(items);
  const { isOpenKeyboard } = useKeyboard();

  const content =
    localItems &&
    localItems
      .map((item, index) => {
        const isFirst = index === 0;
        return (
          <WordTranslationCard
            key={item.localId}
            isFirst={isFirst}
            item={item}
            setLocalItems={setLocalItems}
            setAnswers={setAnswers}
            setTrueAnswers={setTrueAnswers}
          />
        );
      })
      .slice(0, 3)
      .reverse();

  return (
    <View>
      {!isOpenKeyboard && (
        <WordTranslationHeader
          trueAnswersPercent={trueAnswersPercent}
          style={tw`mb-4`}
        />
      )}
      <View style={{ ...tw`relative w-full` }}>{content}</View>
    </View>
  );
};

export default WordTranslationContent;

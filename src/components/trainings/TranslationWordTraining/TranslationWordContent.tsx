import React, { FC } from 'react';
import { View } from 'react-native';
import { useKeyboard } from '../../../hooks/instruments/useKeyboard';
import { useWordTranslationContent } from '../../../hooks/training/useWordTranslationContent';
import WordTranslationCard from './TranslationWordCard';
import WordTranslationHeader from './TranslationWordHeader';
import tw from 'tailwind-react-native-classnames';
import { DictionaryItemDto } from '../../../api';

interface IWordTranslationContentProps {
  items: DictionaryItemDto[];
}

const WordTranslationContent: FC<IWordTranslationContentProps> = ({
  items,
}) => {
  const {
    localItems,
    setLocalItems,
    setAnswers,
    setTrueAnswers,
    trueAnswersPercent,
  } = useWordTranslationContent(items);
  const { isOpenKeyboard } = useKeyboard();

  return (
    <View>
      {!isOpenKeyboard && (
        <WordTranslationHeader
          trueAnswersPercent={trueAnswersPercent}
          style={tw`mb-4`}
        />
      )}
      <View style={{ ...tw`relative w-full` }}>
        {localItems &&
          localItems
            .map((item, index) => {
              const isFirst = index === 0;

              return (
                <WordTranslationCard
                  key={item.localId}
                  isFirst={isFirst}
                  item={item}
                  setLocalItems={setLocalItems}
                  setAnswers={setAnswers}
                  setTrueAnswers={setTrueAnswers}
                />
              );
            })
            .slice(0, 3)
            .reverse()}
      </View>
    </View>
  );
};

export default WordTranslationContent;

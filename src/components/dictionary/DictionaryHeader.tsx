import React, { FC } from 'react'
import { Pressable, View, TextInput } from 'react-native'
import MaterialIcon from '@expo/vector-icons/MaterialIcons'
import tw from 'tailwind-react-native-classnames'
import { useDictionaryProvider } from '../../providers/use/useDictionaryProvider'

const DictionaryHeader: FC = () => {    
    const { search, setSearch } = useDictionaryProvider()

    return (
        <View style={tw`pt-2 flex-row items-center justify-between`}>
        <View style={tw`flex-row w-full h-10 bg-gray-100 items-center justify-between rounded-md`}>
            <TextInput
                placeholder='Поиск'
                onChangeText={ (value) => setSearch(value) }
                value={ search }
                autoCapitalize='none'
                style={{...tw`px-3 py-2  w-5/6`}}
            />
            { !!search.length &&
            <Pressable onPress={() => setSearch('')}>
                <MaterialIcon
                    name='highlight-remove'
                    size={20}
                    style={{ ...tw`w-8 text-gray-400`, height:20 }}
                /> 
            </Pressable>
            }
        </View>
        </View>
    )
}

export default DictionaryHeader
import { useNavigation } from '@react-navigation/native'
import React, { FC } from 'react'
import { Pressable, View, Text } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import { useDictionaryProvider } from '../../providers/use/useDictionaryProvider'
import Button from '../ui/Button'

const NotFoundWrapper: FC = () => {    
    const { navigate } = useNavigation()
    const { search } = useDictionaryProvider()
    return (
        <View style={tw`pt-4`}>
            <Text style={tw`text-center text-gray-500 pb-2`}>В вашем словаре ничего не найдено</Text>
            <Button title='Добавить' onPress={() => navigate('Search', { search })}/>
        </View>
    )
}

export default NotFoundWrapper
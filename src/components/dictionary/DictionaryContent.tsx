import React, { FC } from 'react';
import ContentScrollView from '../ui/ContentScrollView';
import DictionaryItem from './DictionaryItem';
import DictionaryNotFound from './DictionaryNotFound';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import { DictionaryItemDto } from '../../api';

interface IDictionaryContentProps {
  filteredItems: DictionaryItemDto[] | null;
}

const DictionaryContent: FC<IDictionaryContentProps> = ({ filteredItems }) => {
  return (
    <View style={{ ...tw`pt-4` }}>
      <ContentScrollView>
        {filteredItems &&
          filteredItems.reverse().map((item) => (
            <DictionaryItem key={item.localId} item={item} />
          ))}
        {(!filteredItems || !filteredItems.length) && <DictionaryNotFound />}
      </ContentScrollView>
    </View>
  );
};

export default DictionaryContent;

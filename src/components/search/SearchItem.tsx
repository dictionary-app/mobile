import React, { Dispatch, FC, SetStateAction } from 'react';
import { Text, Pressable, Image, View } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';
import tw from 'tailwind-react-native-classnames';
import { useSound } from '../../hooks/useSound';
import { DictionaryItemDto } from '../../api';

interface ISearchItemProps {
  item: DictionaryItemDto;
  selectItem: Dispatch<SetStateAction<DictionaryItemDto | null>>;
  selectedItem: DictionaryItemDto | null;
}

const SearchItem: FC<ISearchItemProps> = ({
  item,
  selectItem,
  selectedItem,
}) => {
  const { playSound, isPlaying } = useSound(item.pronunciation);

  const isSelectedItem = () =>
    item.twordId === selectedItem?.twordId ? 'text-blue-600' : '';

  return (
    <Pressable
      onPress={() => selectItem(item)}
      style={tw`px-3 py-3 border-b border-gray-300 border-solid flex-row items-center justify-between`}
    >
      <Text style={tw`${isSelectedItem()}`}>
        {item.translateValue}
      </Text>
      <View style={tw`flex-row items-center`}>
        {!!item.picture && (
          <Image
            source={{ uri: item.picture }}
            style={{ ...tw`rounded-lg w-10 h-10` }}
          />
        )}
        {!!item.pronunciation && (
          <Pressable onPress={playSound}>
            <Icon
              size={26}
              name="volume-medium"
              style={tw`${isPlaying ? 'text-blue-400' : 'text-gray-600'} pl-4`}
            />
          </Pressable>
        )}
      </View>
    </Pressable>
  );
};

export default SearchItem;

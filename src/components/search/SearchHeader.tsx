import React, { Dispatch, FC, SetStateAction } from 'react';
import { Pressable, View, TextInput } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';
import MaterialIcon from '@expo/vector-icons/MaterialIcons';
import tw from 'tailwind-react-native-classnames';

interface ISearchHeaderProps {
  search: string;
  searchHandler: () => void;
  clearHandler: () => void;
  setSearch: Dispatch<SetStateAction<string>>;
  style?: any;
}

const SearchHeader: FC<ISearchHeaderProps> = ({
  search,
  searchHandler,
  clearHandler,
  setSearch,
  style,
}) => {
  return (
    <View
      style={{ ...tw`pt-2 flex-row items-center justify-between`, ...style }}
    >
      <View
        style={tw`flex-row w-5/6 bg-gray-100 h-10 items-center justify-between rounded-md`}
      >
        <TextInput
          placeholder="Поиск"
          onChangeText={setSearch}
          value={search}
          autoCapitalize="none"
          style={{ ...tw`px-3 py-2  w-5/6` }}
        />
        {!!search.length && (
          <Pressable onPress={clearHandler}>
            <MaterialIcon
              name="highlight-remove"
              size={20}
              style={{ ...tw`w-8 text-gray-400`, height: 20 }}
            />
          </Pressable>
        )}
      </View>
      <Pressable
        style={tw`w-12 h-10 bg-yellow-300 items-center justify-center rounded-md`}
        onPress={searchHandler}
      >
        <Icon name="search" size={20} style={tw`text-gray-600`} />
      </Pressable>
    </View>
  );
};

export default SearchHeader;

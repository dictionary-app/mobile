import React, { FC } from 'react';
import tw from 'tailwind-react-native-classnames';
import { Pressable, View, Text } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';

interface IFooter {
  title: string;
  onBack: () => void;
}

const Footer: FC<IFooter> = ({ children, onBack, title }) => {
  return (
    <View
      style={{
        ...tw`w-full pt-8 px-4 bg-gray-50 flex-row justify-between items-center`,
        borderBottomColor: '#E1E1E1',
        borderBottomWidth: 1,
        height: 80,
      }}
    >
      <Pressable
        style={{
          ...tw`justify-center items-center flex-row h-full`,
          width: 40,
        }}
        onPress={onBack}
      >
        <Icon name="arrow-back" size={28} />
      </Pressable>
      <Text style={tw`text-xl pr-2`}>{title}</Text>
      {children}
      {!children && <View style={tw`w-8`}></View>}
    </View>
  );
};

export default Footer;

import React, { FC } from 'react';
import { useWindowDimensions, ScrollView } from 'react-native';
import tw from 'tailwind-react-native-classnames';

interface IContentScrollView {
  style?: any;
}

const ContentScrollView: FC<IContentScrollView> = ({
  style = {},
  children,
}) => {
  const windowHeight = useWindowDimensions().height;

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{ height: windowHeight - 230 }}
    >
      {children}
    </ScrollView>
  );
};

export default ContentScrollView;

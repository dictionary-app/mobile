import React, { FC } from 'react';
import { TextInput } from 'react-native';
import tw from 'tailwind-react-native-classnames';

interface IField {
  onChange: (val: string) => void;
  val: string;
  placeholder: string;
  isSecure?: boolean;
  style?: any;
}

const Field: FC<IField> = ({
  onChange,
  placeholder,
  val,
  isSecure,
  style = {},
}) => {
  return (
    <TextInput
      placeholder={placeholder}
      onChangeText={onChange}
      value={val}
      secureTextEntry={isSecure}
      autoCapitalize="none"
      style={{ ...tw`rounded-md bg-gray-100 mb-3 p-3 w-full`, ...style }}
    />
  );
};

export default Field;

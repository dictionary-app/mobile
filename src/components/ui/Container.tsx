import React, { FC } from 'react';
import { ScrollView, View } from 'react-native';
import tw from 'tailwind-react-native-classnames';

interface IContainerProps {
  style?: any
  isScrollable?: boolean
}

const Container: FC<IContainerProps> = ({ children, style = {}, isScrollable = false }) => {
  return isScrollable ? 
  <ScrollView>
    <View style={{ ...tw`px-4 pt-6`, ...style }}>{children}</View>
  </ScrollView>
   : 
  <View style={{ ...tw`px-4 pt-6`, ...style }}>{children}</View>
};

export default Container;

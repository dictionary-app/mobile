import React, { FC } from 'react';
import { TouchableHighlight, Text } from 'react-native';
import Loader from '../ui/Loader';
import tw from 'tailwind-react-native-classnames';

interface IButton {
  onPress: () => void;
  title: string;
  textStyle?: string;
  colors?: [string, string];
  isLoading?: Boolean;
  style?: any;
}

const Button: FC<IButton> = ({
  onPress,
  isLoading = false,
  title,
  textStyle = '',
  colors = ['bg-yellow-300', '#FBBF24'],
  style = {},
}) => {
  return (
    <TouchableHighlight
      onPress={onPress}
      underlayColor={colors[1]}
      style={{
        ...tw`${colors[0]} text-gray-800 rounded-md w-full my-4 py-3`,
        ...style,
      }}
    >
      {isLoading ? (
        <Loader size="small" />
      ) : (
        <Text style={tw`text-center ${textStyle}`}>{title}</Text>
      )}
    </TouchableHighlight>
  );
};

export default Button;

import React, { FC, Dispatch } from 'react';
import { View, TextInput, Pressable } from 'react-native';
import MaterialIcon from '@expo/vector-icons/MaterialIcons';
import tw from 'tailwind-react-native-classnames';

interface IWordTranslationInputProps {
  value: string;
  placeholder?: string;
  setValue: Dispatch<React.SetStateAction<string>>;
  isValid: boolean,
  style?: any;
}

const WordTranslationInput: FC<IWordTranslationInputProps> = ({
  value,
  setValue,
  placeholder = '',
  isValid = true,
  style = {},
}) => {
  const borderColor = isValid ? 'gray-300': 'red-500'
  return (
    <View
      style={tw`
        flex-row w-full 
        border 
        border-${borderColor}
        border-solid 
        bg-gray-100 
        h-12 
        items-center 
        justify-between 
        rounded-md relative`
      }
    >
      <TextInput
        placeholder={placeholder}
        onChangeText={setValue}
        value={value}
        autoCapitalize="none"
        style={{ ...tw`px-3  w-5/6 text-lg leading-5` }}
      />
      {!!value.length && (
        <Pressable style={tw`p-4`} onPress={() => setValue('')}>
          <MaterialIcon
            name="highlight-remove"
            size={20}
            style={{ ...tw`w-8 text-gray-400`, height: 20 }}
          />
        </Pressable>
      )}
    </View>
  );
};

export default WordTranslationInput;

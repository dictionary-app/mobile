import React, { FC } from 'react';
import { View, Text, Animated } from 'react-native';
import tw from 'tailwind-react-native-classnames';

interface IWordTranslationSnackbarProps {
  title: string;
  borderColor?: string
  opacity: any
  style?: any;
}

const WordTranslationSnackBar: FC<IWordTranslationSnackbarProps> = ({
  title,
  borderColor = 'green',
  opacity,
  style = {},
}) => {
  return (
    <Animated.View
      pointerEvents='none'
      style={{
        ...tw`absolute w-full h-full flex justify-center items-center`,
        opacity,
        ...style,
      }}
    >
      <View style={tw`bg-${borderColor}-500 rounded-xl px-10 py-3`}>
        <Text style={tw`font-bold text-white`}>{title}</Text>
      </View>
    </Animated.View>
  );
};

export default WordTranslationSnackBar;

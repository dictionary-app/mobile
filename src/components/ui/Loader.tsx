import React, { FC } from 'react';
import { ActivityIndicator } from 'react-native';

interface ILoaderProps {
  style?: any;
  size?: 'large' | 'small';
}

const Loader: FC<ILoaderProps> = ({ style = {}, size = 'large' }) => {
  return <ActivityIndicator size={size} color="#3b82f6" style={style} />;
};

export default Loader;

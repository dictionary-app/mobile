import React, { FC } from 'react';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';

interface ILinearProgressBarProps {
  percent: number,
  style?: any;
  color?: string;
}

const LinearProgressBar: FC<ILinearProgressBarProps> = ({
  color = 'green',
  percent,
  style = {},
}) => {
  return (
    <View style={style}>
      <View style={{ ...tw`w-full relative` }}>
        <View style={tw`absolute rounded-full w-full h-2 bg-gray-200`} />
        <View
          style={{
            ...tw`absolute h-2 bg-${color}-400 ${
              percent === 100 ? 'rounded-full' : 'rounded-l-full'
            }`,
            width: `${percent}%`,
          }}
        />
      </View>
    </View>
  );
};

export default LinearProgressBar;

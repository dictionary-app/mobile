import React, { FC } from 'react';
import { TouchableHighlight } from 'react-native';
import Icon from '@expo/vector-icons/FontAwesome5';
import tw from 'tailwind-react-native-classnames';

interface IAddButton {
  onPress: () => void;
  iconName?: string;
  colors?: [string, string];
  style?: any;
}

const AddButton: FC<IAddButton> = ({
  onPress,
  iconName = 'plus',
  colors = ['bg-blue-600', '#700bbb'],
  style = {},
}) => {
  return (
    <TouchableHighlight
      onPress={onPress}
      underlayColor={colors[1]}
      style={{
        ...tw`${colors[0]} rounded-full flex justify-center items-center w-14 h-14 absolute bottom-6 right-6`,
        ...style,
      }}
    >
      <Icon size={18} color="white" name={iconName} />
    </TouchableHighlight>
  );
};

export default AddButton;

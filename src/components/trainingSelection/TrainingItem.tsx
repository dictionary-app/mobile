import { useNavigation } from '@react-navigation/native';
import React, { FC } from 'react';
import { ImageBackground, Pressable, Text, View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import { ITrainingItem } from '../../models/TrainingModel';

interface ITrainingItemProps {
  training: ITrainingItem;
}

const TrainingItem: FC<ITrainingItemProps> = ({ training }) => {
  const { navigate } = useNavigation();
  return (
    <Pressable
      onPress={() =>
        navigate(training.navigate, { trainingAlias: training.alias })
      }
      style={{
        ...tw`rounded-md overflow-hidden mx-2 w-2/5 mb-6  bg-gray-300`,
        height: 120,
      }}
    >
      <ImageBackground
        source={training.image}
        resizeMode="cover"
        style={tw`w-full h-full justify-end`}
        imageStyle={tw`rounded-md`}
      >
        <View style={tw`bg-gray-50 opacity-70`}>
          <Text
            style={{
              ...tw`font-medium m-2`,
            }}
          >
            {training.title}
          </Text>
        </View>
      </ImageBackground>
    </Pressable>
  );
};

export default TrainingItem;

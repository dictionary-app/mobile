import React, { FC } from 'react';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import { ITrainingItem } from '../../models/TrainingModel';
import TrainingItem from './TrainingItem';

interface ITrainingContentProps {
    trainings: ITrainingItem[]
}

const TrainingContent: FC<ITrainingContentProps> = ({ trainings }) => {
  return (
    <View style={tw`flex-row flex-wrap justify-center`}>
      { trainings.map((training) => (
          <TrainingItem key={training.id} training={training} />
        ))}
    </View>
  );
};

export default TrainingContent;

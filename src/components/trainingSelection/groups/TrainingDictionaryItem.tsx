import React, { FC } from 'react';
import { Pressable, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import tw from 'tailwind-react-native-classnames';
import { getRandomGradient } from '../../../utils/getRandomGradient';
import { BOX_SHADOW } from '../../../styles';

interface IGroupItemProps {
  clickHandler: () => void
}

const GroupsItem: FC<IGroupItemProps> = ({ clickHandler }) => {

  return (
    <Pressable
      onPress={ clickHandler }
      style={{ ...tw`rounded-md overflow-hidden mx-4 w-2/5 mb-6`, height: 135 }}
    >
      <LinearGradient
        colors={getRandomGradient()}
        style={{...tw`w-full h-full items-center justify-center opacity-80` }}
      >
        <Text style={tw`text-white text-center font-bold`}>Словарь</Text>
      </LinearGradient>
    </Pressable>
  );
};

export default GroupsItem;

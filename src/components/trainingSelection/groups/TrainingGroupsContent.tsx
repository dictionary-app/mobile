import React, { FC } from 'react';
import { View } from 'react-native';
import tw from 'tailwind-react-native-classnames';
import { GroupEntity } from '../../../api';
import { DICTIONARY_GROUP } from '../../../models/GroupsModel';
import GroupsItem from '../../groups/items/GroupsItem';
import GroupsNotFound from '../../groups/items/GroupsNotFound';
import TrainingDictionaryItem from './TrainingDictionaryItem';

interface ITrainingGroupsContentProps {
  groups: GroupEntity[];
  clickHandler: (id: number | string) => void;
}

const TrainingGroupsContent: FC<ITrainingGroupsContentProps> = ({
  groups,
  clickHandler,
}) => {
  return (
    <View style={tw`flex-row flex-wrap justify-center`}>
      <TrainingDictionaryItem
        clickHandler={() => clickHandler(DICTIONARY_GROUP)}
      />
      {groups &&
        groups
          .reverse()
          .map((group) => (
            <GroupsItem
              key={group.id}
              group={group}
              clickHandler={clickHandler}
            />
          ))}
      {(!groups || !groups.length) && <GroupsNotFound />}
    </View>
  );
};

export default TrainingGroupsContent;
